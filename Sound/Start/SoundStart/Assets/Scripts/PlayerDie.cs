﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Added this namespace so scene could be loaded in this class

// Checks to see if the player has been hit by an enemy and should die
public class PlayerDie : MonoBehaviour
{
    // Public statics (accessible from anywhere without a need for a reference)
    public static bool s_PlayerAlive = true; // Stores state of player being dead or alive

    // Private variables
    PlayerInputs r_PlayerIns; // Reference to player inputs

    // Start is called before the first frame update
    void Start()
    {
        s_PlayerAlive = true; // Static variable needs resetting at start of the game as it's state persists
        r_PlayerIns = GetComponent<PlayerInputs>(); // Assiging player inputs script to variable
    }

    // Called when object enters collider attached to this gameobject set as trigger
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Enemy") // Checks tag against parameter object to see if it is tagged as an Enemy
        {
            r_PlayerIns.enabled = false; // Turns off player input 
            s_PlayerAlive = false; // Sets Player Alive to false
            Invoke("LoadScene", 4); // Invokes function to load scene again with a 4 second delay
        }
    }

    // Loads scene
    void LoadScene()
    {
        SceneManager.LoadScene(1); // Reloads the current scene as this is at index 1 in the build settings
    }
}
