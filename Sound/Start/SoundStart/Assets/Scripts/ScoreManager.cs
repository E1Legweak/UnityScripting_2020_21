﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Manager class for keeping track of score
public class ScoreManager : MonoBehaviour
{
    // private variables
    int m_CurrentScore = 0; // Stores current score
    static int m_HighScore = 0;

    public Text r_CurrentScoreText;
    public Text r_HighScoreText;
    
    // Start is called before the first frame update
    void Start()
    {
        SetHighScoreText(); // Sets high score text at start of game
    }

    // Update is called once per frame
    void Update()
    {
        SetCurrentScoreText(); // Sets current score
        SetHighScore(); // Sets high score
    }

    // Public function that enables the score to be updated 
    // Function uses a parameter to allow score to be added to total
    public void AddScore(int points)
    {
        m_CurrentScore = m_CurrentScore + points; // Adds parameter, points, to total score
        // Could also be writen as m_CurrentScore += points;
    }

    // Sets UI text to current score value
    void SetCurrentScoreText()
    {
        r_CurrentScoreText.text = "Score: " + m_CurrentScore; // Appends text and value
    }

    // Sets the high score if player is dead and current score is greater.
    void SetHighScore()
    {
        if (PlayerDie.s_PlayerAlive == false)
        {
            if (m_CurrentScore > m_HighScore)
            {
                m_HighScore = m_CurrentScore;
            }
        }
    }

    // Sets high score UI text
    void SetHighScoreText()
    {
        r_HighScoreText.text = "High Score: " + m_HighScore; // Appends text and value
    }
}
