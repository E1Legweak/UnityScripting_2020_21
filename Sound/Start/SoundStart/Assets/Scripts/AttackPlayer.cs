﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Makes the enemy move towards the player
public class AttackPlayer : MonoBehaviour
{
    // Variable available in inspector
    [SerializeField]
    float m_Speed = 1f;

    // Private variables
    Transform r_PlayerLoc; // Stores referece to the player's ship transform
    Vector3 m_LastFramePos; // Stores position of enemy at end of frame

    // Start is called before the first frame update
    void Start()
    {
        // Finds the player's ship in the scene and assigns its Transform to the reference variable
        r_PlayerLoc = GameObject.FindGameObjectWithTag("Player").transform; 
    }

    // Update is called once per frame
    void Update()
    {
        // Checks static variable in Player Die script to see if player is alive
        if (PlayerDie.s_PlayerAlive == true)
        {
            MoveTo(); // If player is alive runs the move function
        }
        else
        {
            transform.position = m_LastFramePos; // If the player is dead, sets the position to where it was at the end of the last frame.
            // This prevents unsightly motion from the enemy at game over
        }
    }

    // Moves enemy towards the player
    void MoveTo()
    {
        // Translates enemy towards the player at the given speed
        transform.position = Vector3.MoveTowards(transform.position, r_PlayerLoc.position, m_Speed * Time.deltaTime);
        // Store position of enemy after move
        m_LastFramePos = transform.position;
    }
}
