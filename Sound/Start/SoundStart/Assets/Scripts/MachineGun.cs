﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Creates bullet at a rate decided by the designer
// Requires a PlayerInputs script to be added to the same GameObject
public class MachineGun : MonoBehaviour
{
    // Variables available in the inspector to be set there
    [SerializeField]
    float m_FireRatePS = 10f; // Number of bullets spawned per second
    [SerializeField]
    GameObject r_Bullet; // Stores reference to bullet prefab
    [SerializeField]
    Transform r_SpawnPoint; // Stores reference to spawn point to spawn bullets

    // Private variables
    PlayerInputs r_PlayerIns; // Stores reference to Player Inputs script
    bool m_SpawnOK = true; // Stores whether spawn should be allowed
    float m_TimeLastSpawn = 0; // Stores time last bullet was spawned

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GetComponent<PlayerInputs>(); // Assigns Player Inputs script to variable
        Physics.IgnoreLayerCollision(8, 9); // Prevents bullets hitting the player (needs to be set up in layer editor)
        Physics.IgnoreLayerCollision(9, 9); // Prevents bullets hitting other bullets (needs to be set up in layer editor)
    }

    // Update is called once per frame
    void Update()
    {
        // Checks static variable in Player Die script to see if player is alive
        if (PlayerDie.s_PlayerAlive == true)
        {
            // If player is alive, allows spawning the happen by running these functions
            AllowSpawn(); 
            SpawnBullet();
        }
    }

    // Spawns bullet
    void SpawnBullet()
    {
        // Checks if player is inputting fire and SpawnOK is true
        // If both conditions are met, a bullet is spawned
        if (m_SpawnOK == true && r_PlayerIns.PrimaryFire == true)
        {
            if (Time.timeScale > 0) // Prevents spawn while paused
            {
                m_TimeLastSpawn = Time.time; // Sets time of last spawn to current game time
                m_SpawnOK = false; // Sets SpawnOK to false to prevent a spawn on the next frame
            
                Instantiate(r_Bullet, r_SpawnPoint.position, r_SpawnPoint.rotation); // Spawns a bullet at the spawn point.
            }
        }
    }

    // Checks time since last spawn
    void AllowSpawn()
    {
        // If enough time has past since last spawn based upon the fire rate, set SpawnOK to true.
        if (Time.time > (m_TimeLastSpawn + (1 / m_FireRatePS)))
        {
            m_SpawnOK = true;
        }
    }
}
