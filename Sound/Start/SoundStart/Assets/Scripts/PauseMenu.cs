﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Added namespace

// Class for pause menu functionality 
public class PauseMenu : MonoBehaviour
{
    public GameObject r_PauseMenu; // Reference to menu object
    PlayerInputs r_PlayerInputs; // Reference to inputs class

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1; // Sets time scale to 1 to ensure game is not paused at scene start
        r_PauseMenu.SetActive(false); // Turns of pause menu so it cannot be seen
        r_PlayerInputs = GameObject.FindObjectOfType<PlayerInputs>(); // Populates the reference with component found in the scene
    }

    // Update is called once per frame
    void Update()
    {
        PauseGame(); // Checks if the game should pause
    }

    // Public function to be called from pause menu button
    public void QuitGame()
    {
        SceneManager.LoadScene(0); // Quits out to start scene
    }

    // Checks whether to pause
    void PauseGame()
    {
        // Checks inputs
        if (r_PlayerInputs.IsPaused == true)
        {
            // Based on time scale, toggles pause on and off
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1; // Sets time scale to play
                r_PauseMenu.SetActive(false); // Turns menu off
            }
            else
            {
                Time.timeScale = 0; // Sets time scale to pause
                r_PauseMenu.SetActive(true); // Turns menu on
            }
        }
    }
}
