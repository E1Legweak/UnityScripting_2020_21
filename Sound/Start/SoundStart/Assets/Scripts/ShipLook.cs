﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Looks ship at a target GameObject
// Requires a PlayerInputs script to be added to the same GameObject
public class ShipLook : MonoBehaviour
{
    // Variable available in inspector for assigning
    [SerializeField]
    Transform r_LookTarget; // Stores the Transform of the target GameObject

    //Private variables
    PlayerInputs r_PlayerIns; // Stores a refernce to the Player Inputs script

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GetComponent<PlayerInputs>(); // Assigns Player Inputs to the variable 
    }

    // Update is called once per frame
    void Update()
    {
        TargetPos(); // Updates the Target Position every frame
    }

    // Sets the position of the target transform
    void TargetPos()
    {
        if (Time.timeScale > 0) // Only runs when not paused
        {
            r_LookTarget.position = r_PlayerIns.LookTarget; // Sets the position from the player input
            transform.LookAt(r_LookTarget); // Makes the object this script is attached to (the ship) look at the target
        }
    }
}
