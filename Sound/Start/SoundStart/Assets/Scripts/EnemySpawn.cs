﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Spawns enemy
public class EnemySpawn : MonoBehaviour
{
    // Variables made available in editor
    [SerializeField]
    float m_SpawnInterval = 1f; // Time between each spawn. This is currently constant but could be made variable
    [SerializeField]
    public GameObject r_Enemy; // Stores enemy prefab from project. This is copied (instantiated from) to create new enemies

    // Private variables
    float m_SpawnTime = 4; // Time till next spawn 

    // Start is called before the first frame update
    void Start()
    {
        m_SpawnTime = Time.time + 4; // Ensures their is a pause in spawn on each level load
    }

    // Update is called once per frame
    void Update()
    {
        // Checks static variable in Player Die script to see if player is alive
        if (PlayerDie.s_PlayerAlive == true)
        {
            SpawnCheck(); // Runs spawn check every frame during game play
        }
    }

    // Assess if enough time has passed to spawn an enemy
    void SpawnCheck()
    {
        if (Time.time > m_SpawnTime) // Checks time
        {
            m_SpawnTime = Time.time + m_SpawnInterval; // Sets time of next spawn
            Spawn(); // Runs function to spawn an enemy
        }
    }

    // Instanciates and enemy in a random location
    void Spawn()
    {
        Vector3 pos = Vector3.zero; // Temporary variable to store spawn location
        int dice = Random.Range(1, 5); // Random Number Generation to choose side

        // Switch statement to set spawn location based on dice
        switch (dice)
        {
            case 1:
                pos = new Vector3(-20, 0, Random.Range(-9.5f, 9.5f)); // Position left at random location
                break;
            case 2:
                pos = new Vector3(20, 0, Random.Range(-9.5f, 9.5f)); // Position right at random location
                break;
            case 3:
                pos = new Vector3(Random.Range(-17f, 17f), 0, -13.5f); // Position bottom at random location
                break;
            case 4:
                pos = new Vector3(Random.Range(-17f, 17f), 0, 13.5f); // Position top at random location
                break;
            default: // This code should never run, but it is good practice to have a default
                pos = new Vector3(Random.Range(-17f, 17f), 0, 13.5f); // Position top at random location
                break;
        }

        Instantiate(r_Enemy, pos, Quaternion.identity); // Creates new enemy at randomised location (keeps rotation the same as prefab)

    }
}
