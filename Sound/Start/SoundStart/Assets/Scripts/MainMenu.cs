﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Added namespace

// Class for main menu functionality
public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Not used
    }

    // Update is called once per frame
    void Update()
    {
        // Not used
    }

    // Public function to be called from menu button
    public void LoadGame()
    {
        SceneManager.LoadScene(1); // Loads gameplay scene
    }

    // Public function to be called from menu button
    public void QuitGame()
    {
        Application.Quit(); // Quits game
    }
}
