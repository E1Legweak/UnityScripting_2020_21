﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketScript : MonoBehaviour
{
    public float m_Force = 10f;

    Rigidbody r_RigidB;
    bool m_RocketLaunched = false;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        LaunchRocket();
    }

    void LaunchRocket()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_RocketLaunched = true;
        }
    }

    private void FixedUpdate()
    {
        if (m_RocketLaunched == true)
        {
            r_RigidB.AddForce(transform.up * m_Force, ForceMode.Force);
        }
    }
}
