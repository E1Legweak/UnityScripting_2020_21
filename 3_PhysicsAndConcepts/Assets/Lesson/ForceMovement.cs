﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceMovement : MonoBehaviour
{
    public float m_ForcePower = 10f;

    Rigidbody r_RigidB;
    InputManager r_Inputs;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB = GetComponent<Rigidbody>();
        r_Inputs = GetComponent<InputManager>();
        //Physics.IgnoreLayerCollision(8, 8);
    }

    public void ApplyImpulseForce(Vector3 dir)
    {
        //r_RigidB.AddForce(dir * m_ForcePower, ForceMode.Impulse);
        r_RigidB.AddForce(dir * m_ForcePower, ForceMode.VelocityChange);
    }

    private void FixedUpdate()
    {
        if (r_Inputs.m_MoveForward == true)
        {
            ApplyContinuousForce(new Vector3(0, 0, 1));
        }
        if (r_Inputs.m_MoveBackward == true)
        {
            ApplyContinuousForce(new Vector3(0, 0, -1));
        }
        if (r_Inputs.m_MoveLeft == true)
        {
            ApplyContinuousForce(new Vector3(-1, 0, 0));
        }
        if (r_Inputs.m_MoveRight == true)
        {
            ApplyContinuousForce(new Vector3(1, 0, 0));
        }
    }

    void ApplyContinuousForce(Vector3 dir)
    {
        r_RigidB.AddForce(dir * m_ForcePower, ForceMode.Force);
        //r_RigidB.AddForce(dir * m_ForcePower, ForceMode.Acceleration);
    }
}
