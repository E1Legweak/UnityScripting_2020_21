﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetVectorBetweenObjects : MonoBehaviour
{
    public GameObject m_Object1;
    public GameObject m_Object2;

    Vector3 m_diff;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DebugDifferenceVector();
    }

    void DebugDifferenceVector()
    {
        m_diff = m_Object1.transform.position - m_Object2.transform.position;
        Debug.Log(m_diff);
    }
}
