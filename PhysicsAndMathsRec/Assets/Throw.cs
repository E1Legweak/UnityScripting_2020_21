﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throw : MonoBehaviour
{
    public float m_Force = 20f;
    Rigidbody r_RB;
    bool pressed = false;

    // Start is called before the first frame update
    void Start()
    {
        r_RB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !pressed)
        {
            pressed = true;
            r_RB.AddForce(transform.forward * m_Force, ForceMode.Impulse);
        }
    }
}
