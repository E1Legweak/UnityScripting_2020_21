﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLaunch : MonoBehaviour
{
    public float m_Power = 10f;

    Rigidbody r_RigidB;
    bool m_Launched = false;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_Launched = true;
        }
    }

    private void FixedUpdate()
    {
        if (m_Launched == true)
        {
            r_RigidB.AddForce(transform.up * m_Power, ForceMode.Force);
        }
    }
}
