﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    ForceMovement r_ForceM; 

    public bool m_MoveForward = false;
    public bool m_MoveBackward = false;
    public bool m_MoveLeft = false;
    public bool m_MoveRight = false;

    // Start is called before the first frame update
    void Start()
    {
        r_ForceM = GetComponent<ForceMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        //Push();
        Move();
    }

    void Push()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            r_ForceM.ApplyImpulseForce(new Vector3( 0, 0, 1));
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            r_ForceM.ApplyImpulseForce(new Vector3( 0, 0, -1));
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            r_ForceM.ApplyImpulseForce(new Vector3(-1, 0, 0));
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            r_ForceM.ApplyImpulseForce(new Vector3(1, 0, 0));
        }
    }

    void Move()
    {
        if (Input.GetKey(KeyCode.W))
        {
            m_MoveForward = true;
        }
        else
        {
            m_MoveForward = false;
        }

        if (Input.GetKey(KeyCode.S))
        {
            m_MoveBackward = true;
        }
        else
        {
            m_MoveBackward = false;
        }

        if (Input.GetKey(KeyCode.A))
        {
            m_MoveLeft = true;
        }
        else
        {
            m_MoveLeft = false;
        }

        if (Input.GetKey(KeyCode.D))
        {
            m_MoveRight = true;
        }
        else
        {
            m_MoveRight = false;
        }
    }
}
