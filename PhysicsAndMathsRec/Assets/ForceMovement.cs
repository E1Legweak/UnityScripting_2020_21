﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceMovement : MonoBehaviour
{
    public float m_Force = 10f;
    Rigidbody r_RigidB;
    InputManager r_InputM;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB = GetComponent<Rigidbody>();
        r_InputM = GetComponent<InputManager>();
        //Physics.IgnoreLayerCollision(8, 9);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (r_InputM.m_MoveForward == true)
        {
            ApplyContinuousForce(new Vector3(0,0,1));
        }
        if (r_InputM.m_MoveBackward == true)
        {
            ApplyContinuousForce(new Vector3(0, 0, -1));
        }
        if (r_InputM.m_MoveLeft == true)
        {
            ApplyContinuousForce(new Vector3(-1, 0, 0));
        }
        if (r_InputM.m_MoveRight == true)
        {
            ApplyContinuousForce(new Vector3(1, 0, 0));
        }
    }

    void ApplyContinuousForce(Vector3 dir)
    {
        r_RigidB.AddForce(dir * m_Force, ForceMode.Force);
    }

    public void ApplyImpulseForce(Vector3 dir)
    {
        r_RigidB.AddForce(dir * m_Force, ForceMode.Impulse);
    }
}
