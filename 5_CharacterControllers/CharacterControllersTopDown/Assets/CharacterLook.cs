﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLook : MonoBehaviour
{
    public Transform m_LookTarget;
    CharacterInputs r_CharacterIns;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        SetLookTargetPos();
    }

    void SetLookTargetPos()
    {
        float x = r_CharacterIns.m_MousePositionInWorld.x;
        float z = r_CharacterIns.m_MousePositionInWorld.z;

        m_LookTarget.position = new Vector3(x, transform.position.y, z);
        LookTowards(m_LookTarget);
    }

    void LookTowards(Transform target)
    {
        transform.LookAt(target);
    }
}
