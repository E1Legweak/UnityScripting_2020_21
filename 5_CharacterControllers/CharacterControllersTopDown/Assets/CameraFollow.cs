﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float m_Smoothness;
    public float m_OffsetAmount;
    public Transform r_FollowTarget;
    Vector3 m_targetOffset = Vector3.zero;
    Vector3 m_Velocity = Vector3.zero;

    public CharacterInputs r_CharacterIns;
    float x = 0;
    float z = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        SetCameraTarget();
        SmoothFollow();
    }

    void SetCameraTarget()
    {
        
        if (r_CharacterIns.m_PlayerMotion.x > 0)
        {
            x = m_OffsetAmount;
        }
        else if (r_CharacterIns.m_PlayerMotion.x < 0)
        {
            x = -m_OffsetAmount;
        }

        if (r_CharacterIns.m_PlayerMotion.z > 0)
        {
            z = m_OffsetAmount;
        }
        else if (r_CharacterIns.m_PlayerMotion.z < 0)
        {
            z = -m_OffsetAmount;
        }

        m_targetOffset = r_FollowTarget.position + new Vector3(x, 0, z);
    }

    void SmoothFollow()
    {
        transform.position = Vector3.SmoothDamp(transform.position, m_targetOffset, ref m_Velocity, m_Smoothness * Time.deltaTime);
    }
}
