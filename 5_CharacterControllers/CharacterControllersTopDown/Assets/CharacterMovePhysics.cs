﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovePhysics : MonoBehaviour
{
    public float m_TopSpeed = 10f;
    public float m_JumpPower = 10f;
    public bool m_IsGrounded;
    Rigidbody r_RigidB;
    CharacterInputs r_CharacterIns;
    Vector3 m_InputVector = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB = GetComponent<Rigidbody>();
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        Inputs();
        GroundCheck();
        Jump();
    }

    void Inputs()
    {
        m_InputVector = new Vector3(r_CharacterIns.m_PlayerMotion.x * m_TopSpeed, r_RigidB.velocity.y, r_CharacterIns.m_PlayerMotion.z * m_TopSpeed);
    }

    void FixedUpdate()
    {
        Movement(m_InputVector);
    }

    void Movement(Vector3 moveVector)
    {
        r_RigidB.velocity = moveVector;
    }

    void GroundCheck()
    {
        Ray ray = new Ray(transform.position, -transform.up);

        if (Physics.Raycast(ray, 0.5f) == true)
        {
            m_IsGrounded = true;
        }
        else
        {
            m_IsGrounded = false;
        }
    }

    void Jump()
    {
        if (r_CharacterIns.m_Jump == true && m_IsGrounded == true)
        {
            r_RigidB.AddForce(Vector3.up * m_JumpPower, ForceMode.Impulse);
        }
    }
}
