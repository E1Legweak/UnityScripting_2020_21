﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInputs : MonoBehaviour
{
    public Vector3 m_PlayerMotion;
    public Vector3 m_MousePositionInWorld;
    public bool m_Jump;
    float m_HorizontalMove = 0;
    float m_VerticalMove = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        HorizontalMoveInputs();
        MousePositionInScene();
        JumpInput();
    }

    void HorizontalMoveInputs()
    {
        m_HorizontalMove = Input.GetAxis("Horizontal");
        m_VerticalMove = Input.GetAxis("Vertical");

        m_PlayerMotion = Vector3.Normalize(new Vector3(m_HorizontalMove, 0, m_VerticalMove));
    }

    void MousePositionInScene()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 25, Color.red);

        if (Physics.Raycast(ray, out hit) == true)
        {
            m_MousePositionInWorld = hit.point;
        }
    }

    void JumpInput()
    {
        if (Input.GetButtonDown("Jump"))
        {
            m_Jump = true;
        }
        else
        {
            m_Jump = false;
        }
    }
}
