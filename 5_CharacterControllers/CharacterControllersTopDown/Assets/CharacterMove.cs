﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour
{
    public float m_MoveSpeed = 10f;
    public float m_JumpPower = 10f;
    public float m_JumpTime = 0.3f;
    public float m_Gravity = 9.81f;
    public bool m_IsGrounded;
    public bool m_IsJumping;
    CharacterController r_CharacterCon;
    CharacterInputs r_CharacterIns;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterCon = GetComponent<CharacterController>();
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        Gravity();
        Movement(r_CharacterIns.m_PlayerMotion);
    }

    void Movement(Vector3 moveVector)
    {
        if (m_IsJumping == true)
        {
            moveVector.y += m_JumpPower;
        }
        else if (m_IsGrounded == false)
        {
            moveVector.y += -m_Gravity;
        }
        r_CharacterCon.Move(moveVector * Time.deltaTime * m_MoveSpeed);
    }

    void Gravity()
    {
        Ray ray = new Ray(transform.position, -transform.up);

        if (Physics.Raycast(ray, 0.2f) == true)
        {
            m_IsGrounded = true;
        }
        else
        {
            m_IsGrounded = false;
        }
    }

    void Jump()
    {
        if (r_CharacterIns.m_Jump == true && m_IsGrounded == true)
        {
            m_IsJumping = true;
            CancelInvoke("StopJump");
            Invoke("StopJump", m_JumpTime);
        }
    }

    void StopJump()
    {
        m_IsJumping = false;
    }
}
