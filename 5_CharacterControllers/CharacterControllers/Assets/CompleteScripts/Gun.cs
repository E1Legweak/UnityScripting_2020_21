﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public Transform spawnPoint; //reference to spawn point
    public GameObject bullet; //reference to bullet prefab
    public float fireInterval = 0.5f; //time interval between shots

    float lastFired = 0; //stores time last bullet was spawned

    // Update is called once per frame
    void Update()
    {
        Inputs();
    }

    //gets player input
    void Inputs()
    {
        if (Input.GetButton("Fire1"))
        {
            MachineGun();
        }
    }

    //repeatedly fires bullets
    void MachineGun()
    {
        if (Time.time > lastFired + fireInterval)
        {
            lastFired = Time.time;
            Instantiate(bullet, spawnPoint.position, spawnPoint.rotation);
        }
    }
}
