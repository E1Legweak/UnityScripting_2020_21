﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsMovement : MonoBehaviour
{
    public float speed = 10; //Speed of movement
    public float mouseSens = 2; //mouse sensitivity

    Rigidbody _rb; //reference to rigidbody
    Vector3 motionVector = Vector3.zero; //stores motion from inputs
    Vector3 rotateVector = Vector3.zero; //stores rotation from inputs
    float sideMotion = 0; // stores input axis for movement
    float rotateMotion = 0; //stores input axis for rotation

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveInputs();        
        MoveRotate();
    }

    private void FixedUpdate()
    {
        Motion();
    }

    //Gets inputs for movement
    void MoveInputs()
    {
        sideMotion = Input.GetAxis("Horizontal");
        if (transform.position.x > 6)
        {
            sideMotion = Mathf.Clamp(sideMotion, -1, 0);
            if (_rb.velocity.x > 0)
            {
                _rb.velocity = Vector3.zero;
            }
        }
        else if (transform.position.x < -6)
        {
            sideMotion = Mathf.Clamp(sideMotion, 0, 1);
            if (_rb.velocity.x < 0)
            {
                _rb.velocity = Vector3.zero;
            }
        }
        motionVector = new Vector3(sideMotion, 0, 0);
    }

    // moves ship
    void Motion()
    {
        _rb.AddForce(motionVector * speed, ForceMode.Force);
    }

    //gets rotation inputs
    void MoveRotate()
    {
        rotateMotion = Input.GetAxis("Mouse X");

        if (transform.localEulerAngles.y < 300 && transform.localEulerAngles.y > 180)
        {
            rotateMotion = Mathf.Clamp(rotateMotion, 0, 1);
        }
        else if (transform.localEulerAngles.y > 60 && transform.localEulerAngles.y < 180)
        {
            rotateMotion = Mathf.Clamp(rotateMotion, -1, 0);
        }
        rotateVector = new Vector3(0, rotateMotion * mouseSens, 0);
        transform.Rotate(rotateVector, Space.Self);
    }
}
