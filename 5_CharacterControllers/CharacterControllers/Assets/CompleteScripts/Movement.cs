﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    //public variables
    public float speed = 10; //Speed of movement
    public float mouseSens = 2; //mouse sensitivity

    CharacterController _cc; //reference to character controller
    Vector3 motionVector = Vector3.zero; //stores motion from inputs
    Vector3 rotateVector = Vector3.zero; //stores rotation from inputs
    float sideMotion = 0; // stores input axis for movement
    float rotateMotion = 0; //stores input axis for rotation

    // Start is called before the first frame update
    void Start()
    {
        _cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveInputs();
        RotateInputs();
        Motion();
    }

    //Gets inputs for movement
    void MoveInputs()
    {
        sideMotion = Input.GetAxis("Horizontal");
        if (transform.position.x > 6)
        {
            sideMotion = Mathf.Clamp(sideMotion, - 1, 0);
        }
        else if (transform.position.x < -6)
        {
            sideMotion = Mathf.Clamp(sideMotion, 0, 1);
        }
        motionVector = new Vector3(sideMotion, 0, 0);
    }

    // moves ship
    void Motion()
    {
        _cc.Move(motionVector * Time.deltaTime * speed);
        transform.Rotate(rotateVector, Space.Self);
    }

    //gets rotation inputs
    void RotateInputs()
    {
        rotateMotion = Input.GetAxis("Mouse X");

        if (transform.localEulerAngles.y < 300 && transform.localEulerAngles.y > 180)
        {
            rotateMotion = Mathf.Clamp(rotateMotion, 0, 1);
        }
        else if (transform.localEulerAngles.y > 60 && transform.localEulerAngles.y < 180)
        {
            rotateMotion = Mathf.Clamp(rotateMotion, -1, 0);
        }

        rotateVector = new Vector3(0, rotateMotion * mouseSens, 0);
    }
}
