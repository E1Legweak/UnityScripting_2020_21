﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public float moveSpeed = 1; //enemy move speed
    GameObject player; //reference to player
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player"); //assigns reference to player
    }

    // Update is called once per frame
    void Update()
    {
        //if player exists, moves towards player
        if (player != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, 
                player.transform.position, moveSpeed * Time.deltaTime);
        }
    }
}
