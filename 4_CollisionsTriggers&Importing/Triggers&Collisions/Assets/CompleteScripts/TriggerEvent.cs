﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    AudioSource r_AudioS;
    bool m_EventTriggered;
    Throw r_Throw;
    
    // Start is called before the first frame update
    void Start()
    {
        r_AudioS = GetComponent<AudioSource>();
        r_Throw = GameObject.Find("Scare").GetComponent<Throw>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && m_EventTriggered == false)
        {
            Debug.Log("Player entered");
            r_AudioS.Play();
            r_Throw.ScareEvent();
            m_EventTriggered = true;
        }
    }
}
