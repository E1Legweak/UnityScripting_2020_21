﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSound : MonoBehaviour
{
    AudioSource r_AudioS;
    bool m_SoundPlayed;

    // Start is called before the first frame update
    void Start()
    {
        r_AudioS = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && m_SoundPlayed == false)
        {
            Debug.Log("Player entered");
            r_AudioS.Play();
            m_SoundPlayed = true;
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            Debug.Log("Player exited");
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.tag == "Player")
        {
            Debug.Log("Player in trigger");
        }
    }
}
