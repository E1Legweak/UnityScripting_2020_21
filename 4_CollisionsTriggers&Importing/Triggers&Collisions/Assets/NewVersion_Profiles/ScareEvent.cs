﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScareEvent : MonoBehaviour
{
    public float m_Force = 1;

    bool m_PlayedSound;
    Rigidbody r_RigidB;
    AudioSource r_AudioS;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB = GetComponent<Rigidbody>();
        r_AudioS = GetComponent<AudioSource>();
    }

    void TriggeredEventEnter()
    {
        r_RigidB.useGravity = true;
        r_RigidB.AddForce(transform.forward * m_Force, ForceMode.Impulse);
    }

    void OnCollisionEnter()
    {
        if (m_PlayedSound == false)
        {
            r_AudioS.Play();
            m_PlayedSound = true;
        }
    }
}
