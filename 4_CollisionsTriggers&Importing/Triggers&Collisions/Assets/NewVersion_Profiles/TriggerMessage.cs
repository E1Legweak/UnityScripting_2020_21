﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMessage : MonoBehaviour
{
    public bool m_UseEnter;
    public bool m_UseExit;
    public bool m_UseStay;
    public string m_ActiveTag;
    public GameObject r_TargetObject;

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == m_ActiveTag && m_UseEnter)
        {
            if (r_TargetObject != null)
            {
                r_TargetObject.BroadcastMessage("TriggeredEventEnter");
            }
            else
            {
                BroadcastMessage("TriggeredEventEnter");
            }
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag == m_ActiveTag && m_UseExit)
        {
            if (r_TargetObject != null)
            {
                r_TargetObject.BroadcastMessage("TriggeredEventExit");
            }
            else
            {
                BroadcastMessage("TriggeredEventExit");
            }
        }
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.tag == m_ActiveTag && m_UseStay)
        {
            if (r_TargetObject != null)
            {
                r_TargetObject.BroadcastMessage("TriggeredEventExit");
            }
            else
            {
                BroadcastMessage("TriggeredEventExit");
            }
        }
    }
}
