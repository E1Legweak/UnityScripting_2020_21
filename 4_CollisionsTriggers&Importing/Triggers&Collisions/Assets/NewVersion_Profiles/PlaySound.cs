﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    AudioSource r_AudioS;
    bool m_soundPlayed = false;


    // Start is called before the first frame update
    void Start()
    {
        r_AudioS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void TriggeredEventEnter()
    {
        if (m_soundPlayed == false)
        {
            r_AudioS.Play();
            m_soundPlayed = true;
        }
    }
}
