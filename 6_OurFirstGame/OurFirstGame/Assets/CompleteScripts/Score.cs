﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public int currentScore = 0;

    // Adds Score
    public void AddScore()
    {
        currentScore = currentScore + 10; //adds 10 to current score
        Debug.Log("Player Score = " + currentScore);
    }

    //Message when player dies
    public void PlayerDied()
    {
        Debug.Log("Player Died. Final Score = " + currentScore + ". Play again?");
    }
}
