﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public float spawnInterval = 1; //time interval between spawns
    public GameObject enemy; //enemy prefab that is spawned

    float lastSpawn = 0; //stores time of last spawn
    GameObject player; //reference to player

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player"); //gets reference to player
    }
    // Update is called once per frame
    void Update()
    {
        //checks if player exists and spawns if so
        if (player != null)
        {
            Spawn();
        }
    }

    //spawns enemies based upon an interval of time
    void Spawn()
    {
        //tests if the correct amount of time has passed since the last spawn
        if(Time.time > lastSpawn + spawnInterval)
        {
            Vector3 spawnLocation = new Vector3(12, 0.5f, Random.Range(-7, 7)); //generates random location
            Instantiate(enemy, spawnLocation, Quaternion.Euler(0, -90, 0)); //creates enemy
            lastSpawn = Time.time; //stores time of spawn
        }
    }
}
