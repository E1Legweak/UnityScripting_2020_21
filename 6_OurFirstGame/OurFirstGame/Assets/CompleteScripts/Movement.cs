﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    //public variables
    public float speed = 10; //Speed of movement

    //private variables
    CharacterController _cc; //reference to character controller
    Vector3 motionVector = Vector3.zero; //stores motion from inputs
    float zMotion = 0; // stores input axis for movement

    // Start is called before the first frame update
    void Start()
    {
        _cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        //Calls the method dealing with Inputs first, then the one dealing woth Motion
        MoveInputs();
        Motion();
    }

    //Gets inputs for movement
    void MoveInputs()
    {
        //stores Vertical axis in variable zMotion
        zMotion = Input.GetAxis("Vertical");

        //clamps zMotion based upon the player's ship's position
        if (transform.position.z > 7)
        {
            zMotion = Mathf.Clamp(zMotion, - 1, 0);
        }
        else if (transform.position.z < -7)
        {
            zMotion = Mathf.Clamp(zMotion, 0, 1);
        }

        //adds zMotion into a vector3 so it can be used to move the player
        motionVector = new Vector3(0, 0, zMotion);
    }

    // moves ship
    void Motion()
    {
        //moves the player using the Move method in the CharacterController
        _cc.Move(motionVector * Time.deltaTime * speed);
    }
}
