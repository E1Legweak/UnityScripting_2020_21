﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    //public varibales
    public Transform spawnPoint; //reference to spawn point
    public GameObject bullet; //reference to bullet prefab

    //private variables
    float lastFired = 0; //stores time last bullet was spawned

    // Update is called once per frame
    void Update()
    {
        //Inputs is run every frame
        Inputs();
    }

    //gets player input
    void Inputs()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            //creates bullet at spawn point's world location and with its world rotation 
            Instantiate(bullet, spawnPoint.position, spawnPoint.rotation);
        }
    }
}
