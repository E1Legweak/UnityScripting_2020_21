﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDie : MonoBehaviour
{
    public static bool s_PlayerAlive = true;
    PlayerInputs r_PlayerIns;

    // Start is called before the first frame update
    void Start()
    {
        s_PlayerAlive = true;
        r_PlayerIns = GetComponent<PlayerInputs>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Enemy")
        {
            r_PlayerIns.enabled = false;
            s_PlayerAlive = false;
            Invoke("LoadScene", 4);
            //Destroy(gameObject);
        }
    }

    void LoadScene()
    {
        SceneManager.LoadScene(0);
    }
}
