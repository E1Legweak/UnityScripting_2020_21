﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipLook : MonoBehaviour
{
    public Transform m_LookTarget;

    PlayerInputs r_PlayerIns;

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GetComponent<PlayerInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        TargetPos();
    }

    void TargetPos()
    {
        m_LookTarget.position = r_PlayerIns.m_LookTarget;
        transform.LookAt(m_LookTarget);
    }
}
