﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    public float m_ShipSpeed = 1f;
    Vector3 m_ClampedMotion = Vector3.zero;
    PlayerInputs r_PlayerIns;
    CharacterController r_CharacterCon;

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GetComponent<PlayerInputs>();
        r_CharacterCon = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerDie.s_PlayerAlive == true)
        {
            PlayerMove();
        }
    }

    void PlayerMove()
    {
        m_ClampedMotion = r_PlayerIns.m_PlayerAxis;

        if (transform.position.x < -17)
        {
            m_ClampedMotion.x = Mathf.Clamp(m_ClampedMotion.x, 0, 1);
            transform.position = new Vector3(-17, 0, transform.position.z);
        }
        else if (transform.position.x > 17)
        {
            m_ClampedMotion.x = Mathf.Clamp(m_ClampedMotion.x, -1, 0);
            transform.position = new Vector3(17, 0, transform.position.z);
        }

        if (transform.position.z < -9.5f)
        {
            m_ClampedMotion.z = Mathf.Clamp(m_ClampedMotion.z, 0, 1);
            transform.position = new Vector3(transform.position.x, 0, -9.5f);
        }
        else if (transform.position.z > 9.5f)
        {
            m_ClampedMotion.z = Mathf.Clamp(m_ClampedMotion.z, -1, 0);
            transform.position = new Vector3(transform.position.x, 0, 9.5f);
        }


        r_CharacterCon.Move(m_ClampedMotion * m_ShipSpeed);
    }
}
