﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : MonoBehaviour
{
    public float m_AxisSmooth = 1f;
    public Vector3 m_PlayerAxis = Vector3.zero;
    public Vector3 m_LookTarget = Vector3.zero;
    public bool m_PrimaryFire = false;

    Vector3 m_PlayerAxisRaw = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveInputs();
        LookInputs();
        PrimeFire();
    }

    void MoveInputs()
    {
        m_PlayerAxisRaw = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        m_PlayerAxisRaw = Vector3.Normalize(m_PlayerAxisRaw);

        m_PlayerAxis = Vector3.Lerp(m_PlayerAxis, m_PlayerAxisRaw, m_AxisSmooth * Time.deltaTime);
    }

    void LookInputs()
    {
        m_LookTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_LookTarget = new Vector3(m_LookTarget.x, 0, m_LookTarget.z);
    }

    void PrimeFire()
    {
        m_PrimaryFire = Input.GetButton("Fire1");
    }

}
