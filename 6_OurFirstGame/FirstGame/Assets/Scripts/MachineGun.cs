﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : MonoBehaviour
{
    public float m_FireRatePerSec = 10f;
    public GameObject r_Bullet;
    public Transform r_SpawnPoint;

    PlayerInputs r_PlayerIns;
    bool m_SpawnOk = true;
    float m_TimeLastSpawn = 0f;

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GetComponent<PlayerInputs>();
        Physics.IgnoreLayerCollision(8, 9);
        Physics.IgnoreLayerCollision(9, 9);
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerDie.s_PlayerAlive == true)
        {
            AllowSpawn();
            SpawnBullet();
        }
    }

    void SpawnBullet()
    {
        if (r_PlayerIns.m_PrimaryFire == true && m_SpawnOk == true)
        {
            Instantiate(r_Bullet, r_SpawnPoint.position, r_SpawnPoint.rotation);
            m_TimeLastSpawn = Time.time;
            m_SpawnOk = false;
        }
    }

    void AllowSpawn()
    {
        if (Time.time > (m_TimeLastSpawn + (1/m_FireRatePerSec)))
        {
            m_SpawnOk = true;
        }
    }
}
