﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float m_SpawnInterval = 2f;
    public GameObject r_Enemy;
    float m_SpawnTime = 4;

    // Start is called before the first frame update
    void Start()
    {
        m_SpawnTime = Time.time + 4;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerDie.s_PlayerAlive == true)
        {
            SpawnCheck();
        }
    }


    void SpawnCheck()
    {
        if (Time.time > m_SpawnTime)
        {
            m_SpawnTime = Time.time + m_SpawnInterval;
            Spawn();
        }
    }

    void Spawn()
    {
        Vector3 pos = Vector3.zero;
        int dice = Random.Range(1, 5);
        switch (dice)
        {
            case 1:
                pos = new Vector3(Random.Range(-20.5f, -17.5f), 0, Random.Range(-9.5f, 9.5f));
                break;
            case 2:
                pos = new Vector3(Random.Range(17.5f, 20.5f), 0, Random.Range(-9.5f, 9.5f));
                break;
            case 3:
                pos = new Vector3(Random.Range(-17.5f, 17.5f), 0, Random.Range( 9.5f, 13.5f));
                break;
            case 4:
                pos = new Vector3(Random.Range(-17, 17), 0, Random.Range(-13.5f, -9.5f));
                break;
            default:
                pos = new Vector3(Random.Range(-17.5f, 17.5f), 0, Random.Range(9.5f, 13.5f));
                break;
        }
        Instantiate(r_Enemy, pos, Quaternion.identity);
    }
}
