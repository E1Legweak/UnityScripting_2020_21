﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPlayer : MonoBehaviour
{
    public float m_Speed = 1f;
    Transform r_PlayerLoc;

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerLoc = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerDie.s_PlayerAlive == true)
        {
            MoveTo();
        }
    }

    void MoveTo()
    {
        if (r_PlayerLoc != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, r_PlayerLoc.position, m_Speed * Time.deltaTime);
        }
    }
}
