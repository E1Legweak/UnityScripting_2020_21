﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDie : MonoBehaviour
{
    ScoreManager r_ScoreMan;
    // Start is called before the first frame update
    void Start()
    {
        r_ScoreMan = GameObject.Find("GameManager").GetComponent<ScoreManager>();
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            r_ScoreMan.AddScore(10);
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }
}
