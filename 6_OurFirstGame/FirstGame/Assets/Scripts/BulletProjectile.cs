﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    public Rigidbody r_RigidB;
    public float m_BulletSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB.AddForce(transform.forward * m_BulletSpeed, ForceMode.VelocityChange);
        Destroy(gameObject, 2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
