﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotion : MonoBehaviour
{
    public float m_Speed = 1f;
    public float m_SmoothMotion = 1f;

    Vector3 m_InputSmoothed = Vector3.zero;
    Vector3 m_CurrentVel = Vector3.zero;

    CharacterController r_CharacterCon;
    CharacterInputs r_CharacterIns;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterCon = GetComponent<CharacterController>();
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMotion();
    }

    void PlayerMotion()
    {
        m_InputSmoothed = Vector3.SmoothDamp(m_InputSmoothed, r_CharacterIns.m_PlayerMoveVect, 
            ref m_CurrentVel, m_SmoothMotion * Time.deltaTime);
        r_CharacterCon.Move(m_InputSmoothed * (m_Speed / 10));
    }
}
