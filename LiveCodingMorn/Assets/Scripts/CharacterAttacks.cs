﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttacks : MonoBehaviour
{
    public Transform r_LookTarget;

    CharacterInputs r_CharacterIns;
    bool m_WhipFired = false;
    int m_layermask1 = 1 << 8;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        FireWhip();
    }

    void FireWhip()
    {
        if (r_CharacterIns.m_SpecialToggle == true && r_CharacterIns.m_FireToggle == true)
        {
            if (m_WhipFired == false)
            {
                m_WhipFired = true;
                Whip();
            }
        }
        else if (r_CharacterIns.m_FireToggle == false)
        {
            m_WhipFired = false;
        }

    }

    void Whip()
    {
  
        RaycastHit[] raycastHits;
        raycastHits = Physics.SphereCastAll(r_LookTarget.position, 0.1f, -transform.up, 5, m_layermask1);
        Debug.Log(raycastHits.Length);

        if (raycastHits.Length > 0)
        {
            //Destroy(raycastHits[0].collider.gameObject);
            raycastHits[0].collider.transform.SetParent(transform);
        }


        /*Ray ray = 
        if (Physics.SphereCastALL(r_LookTarget.position, 5f, transform.TransformDirection(Vector3.forward), out raycastHit))
        {
            Debug.Log("Shit happens");
        }*/
    }


}
