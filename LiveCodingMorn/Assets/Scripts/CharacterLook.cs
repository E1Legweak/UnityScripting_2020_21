﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLook : MonoBehaviour
{
    public GameObject r_Target;

    CharacterInputs r_CharacterIns;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        SetTargetPos();
        LookAtTarget();
    }

    void SetTargetPos()
    {
        r_Target.transform.position = r_CharacterIns.m_LookPos;
    }

    void LookAtTarget()
    {
        if (r_Target != null)
        {
            transform.LookAt(r_Target.transform);
        }
        else
        {
            Debug.LogWarning("Look target is unassigned");
        }
    }
}
