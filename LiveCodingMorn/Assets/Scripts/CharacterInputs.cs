﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInputs : MonoBehaviour
{
    public Vector3 m_PlayerMoveVect = Vector3.zero;
    public Vector3 m_LookPos = Vector3.zero;
    public bool m_SpecialToggle = false;

    public bool m_FireToggle = false;
    public bool m_DefenceToggle = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MotionVector();
        DefenceToggle();
        SpecialToggle();
        FireToggle();
        CharacterLook();
    }

    void MotionVector()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        m_PlayerMoveVect = new Vector3(x, 0, y);
    }

    void SpecialToggle()
    {
        if (Input.GetButtonDown("Special"))
        {
            m_SpecialToggle = true;
        }
        else if (Input.GetButtonUp("Special"))
        {
            m_SpecialToggle = false;
        }
    }

    void CharacterLook()
    {
        m_LookPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_LookPos = new Vector3(m_LookPos.x, transform.position.y, m_LookPos.z);
    }

    void FireToggle()
    {
        if (m_DefenceToggle == false)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                m_FireToggle = true;
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                m_FireToggle = false;
            }
        }
    }

    void DefenceToggle()
    {
        if (Input.GetButtonDown("Defence"))
        {
            m_DefenceToggle = true;
        }
        else if (Input.GetButtonUp("Defence"))
        {
            m_DefenceToggle = false;
        }
    }

}
