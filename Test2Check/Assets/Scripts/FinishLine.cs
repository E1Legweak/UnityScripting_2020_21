﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    public EventManager r_EventManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "HorseBrown")
        {
            r_EventManager.m_Takings += 10000;
            r_EventManager.m_NumberOfHorsesFinished++;
            Destroy(col.gameObject, 2f);
            Debug.Log(r_EventManager.m_NumberOfHorsesFinished + " horses have raced today. The bookmakers have made £" + r_EventManager.m_Takings + " in profit");
        }

        if (col.tag == "HorseGrey")
        {
            r_EventManager.m_Takings += 20000;
            r_EventManager.m_NumberOfHorsesFinished++;
            Destroy(col.gameObject, 2f);
            Debug.Log(r_EventManager.m_NumberOfHorsesFinished + " horses have raced today. The bookmakers have made £" + r_EventManager.m_Takings + " in profit");
        }
    }
}
