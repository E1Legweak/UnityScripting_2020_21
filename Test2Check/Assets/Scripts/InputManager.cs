﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public bool m_EventOpen;
    public bool m_SpawnBrownHorse;
    public bool m_SpawnGreyHorse;
    public bool m_StartGun;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        OpenEvent();
        SpawnHorses();
        StartRace();
    }

    void OpenEvent()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_EventOpen = !m_EventOpen;
            Debug.Log("Event open = " + m_EventOpen);
        }
    }

    void SpawnHorses()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_SpawnBrownHorse = true;
        }
        else
        {
            m_SpawnBrownHorse = false;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_SpawnGreyHorse = true;
        }
        else
        {
            m_SpawnGreyHorse = false;
        }
    }

    void StartRace()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_StartGun = true;
        }
        else
        {
            m_StartGun = false;
        }
    }
}
