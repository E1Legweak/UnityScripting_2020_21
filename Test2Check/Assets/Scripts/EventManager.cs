﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public GameObject r_SpawnPoint1;
    public GameObject r_SpawnPoint2;

    public GameObject r_HorseBrown;
    public GameObject r_HorseGrey;

    GameObject r_HorseBrownSpawn;
    GameObject r_HorseGreySpawn;

    InputManager r_InputManager;

    public int m_NumberOfHorsesFinished;
    public int m_Takings;

    // Start is called before the first frame update
    void Start()
    {
        r_InputManager = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        CreateHorses();
    }

    void CreateHorses()
    {
        if (r_InputManager.m_EventOpen)
        {
            if (r_InputManager.m_SpawnBrownHorse && r_HorseBrownSpawn == null)
            {
                r_HorseBrownSpawn = Instantiate(r_HorseBrown, r_SpawnPoint1.transform.position, r_SpawnPoint1.transform.rotation);
                Debug.Log("Spawn brown horse");
            }

            if (r_InputManager.m_SpawnGreyHorse && r_HorseGreySpawn == null)
            {
                r_HorseGreySpawn = Instantiate(r_HorseGrey, r_SpawnPoint2.transform.position, r_SpawnPoint2.transform.rotation);
                Debug.Log("Spawn grey horse");
            }
        }
        else
        {
            m_NumberOfHorsesFinished = 0;
            m_Takings = 0;
        }
    }
}
