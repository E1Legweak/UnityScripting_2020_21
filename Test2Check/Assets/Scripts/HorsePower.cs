﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorsePower : MonoBehaviour
{
    Rigidbody r_Rigidbody;

    bool m_Run;
    float m_Acceleration;
    InputManager r_InputManager;
    float m_JumpTime;

    // Start is called before the first frame update
    void Start()
    {
        m_Acceleration = Random.Range(0f, 21f);
        r_Rigidbody = GetComponent<Rigidbody>();
        r_InputManager = GameObject.Find("Stable").GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        RaceStart();
        Jump();
    }

    void RaceStart()
    {
        if (r_InputManager.m_StartGun)
        {
            m_Run = true;
        }
    }

    private void FixedUpdate()
    {
        HorseAcceleration();
    }

    void HorseAcceleration()
    {
        if (m_Run)
        {
            if (m_Acceleration < 20)
            {
                m_Acceleration += 0.1f;
            }

            r_Rigidbody.AddForce(transform.forward * m_Acceleration, ForceMode.Acceleration);
        }
    }

    void Jump()
    {
        if (Time.time > m_JumpTime)
        {
            r_Rigidbody.AddForce(transform.up * 1f, ForceMode.Impulse);
            m_JumpTime = Time.time + Random.Range(1f, 3f);
        }
    }
}
