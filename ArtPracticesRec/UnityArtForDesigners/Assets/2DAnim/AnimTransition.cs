﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimTransition : MonoBehaviour
{
    Animator r_Anim;

    // Start is called before the first frame update
    void Start()
    {
        r_Anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            r_Anim.SetTrigger("EnterRun");
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            r_Anim.SetTrigger("EnterIdle");
        }
    }
}
