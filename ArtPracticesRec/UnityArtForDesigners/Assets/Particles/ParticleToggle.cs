﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleToggle : MonoBehaviour
{
    ParticleSystem r_PartSys;

    // Start is called before the first frame update
    void Start()
    {
        r_PartSys = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (r_PartSys.isPlaying == false)
            {
                r_PartSys.Play();
                
            }
            else
            {
                r_PartSys.Stop();
            }
        }
    }
}
