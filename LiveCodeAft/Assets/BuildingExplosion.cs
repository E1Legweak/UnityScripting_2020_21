﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingExplosion : MonoBehaviour
{
    public GameObject r_NormalHouse;
    public GameObject r_ExplodeHouse;

    public Rigidbody[] r_RBs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Explode();
        }
    }

    void Explode()
    {
        r_NormalHouse.SetActive(false);
        r_ExplodeHouse.SetActive(true);

        foreach (Rigidbody item in r_RBs)
        {
            item.AddExplosionForce(1000, new Vector3(-0.17f, 1.62f, 17.92f), 100);
        }
    }
}
