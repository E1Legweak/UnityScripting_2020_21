﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotation : MonoBehaviour
{
    public GameObject r_CharacterHead;
    public GameObject r_LookTarget;

    CharacterInputs r_CharacterIns;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        SetTargetPos();
        LookAt();
    }

    void SetTargetPos()
    {
        r_LookTarget.transform.position = new Vector3(r_CharacterIns.m_MousePos.x, r_CharacterHead.transform.position.y, r_CharacterIns.m_MousePos.z);
    }

    void LookAt()
    {
        r_CharacterHead.transform.LookAt(r_LookTarget.transform);
    }
}
