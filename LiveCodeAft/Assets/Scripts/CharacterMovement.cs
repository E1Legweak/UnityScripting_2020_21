﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float m_Speed = 1f;
    public float m_BoostSpeed = 2f;
    public float m_BoostDuration = 0.5f;
    public float m_BoostCooldown = 5f;

    int m_LastDir = 1;

    CharacterInputs r_CharacterIns;
    bool m_BoostActive = false;
    bool m_BoostReady = true;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        BoostActivation();
        Motion();
    }

    void BoostActivation()
    {
        if (r_CharacterIns.m_Boost == true && m_BoostReady == true)
        {
            m_BoostActive = true;
            m_BoostReady = false;


            CancelInvoke("BoostDiactivation");
            Invoke("BoostDiactivation", m_BoostDuration);

            CancelInvoke("MakeBoostReady");
            Invoke("MakeBoostReady", m_BoostCooldown);
        }
    }

    void BoostDiactivation()
    {
        m_BoostActive = false;
    }

    void MakeBoostReady()
    {
        m_BoostReady = true;
    }


    void Motion()
    {
        SetLastDirection();
        if (m_BoostActive)
        {
            transform.position = transform.position + m_LastDir * (m_BoostSpeed * (transform.forward * (m_Speed * Time.deltaTime)));
        }
        else
        {
            transform.position = transform.position + (r_CharacterIns.m_CharacterMove * (transform.forward * (m_Speed * Time.deltaTime)));
        }
    }

    void SetLastDirection()
    {
        if (r_CharacterIns.m_CharacterMove > 0)
        {
            m_LastDir = 1;
        }
        else if (r_CharacterIns.m_CharacterMove < 0)
        {
            m_LastDir = -1;
        }
    }
}
