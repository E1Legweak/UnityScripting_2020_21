﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRotation : MonoBehaviour
{
    public float m_Speed = 1f;

    CharacterInputs r_CharacterIns;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        Vector3 rotTarget = transform.rotation.eulerAngles + new Vector3(0, r_CharacterIns.m_CharacterRotation * m_Speed * Time.deltaTime, 0);

        transform.rotation = Quaternion.Euler(rotTarget);
    }
}
