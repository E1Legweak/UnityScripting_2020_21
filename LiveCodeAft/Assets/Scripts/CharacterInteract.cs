﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInteract : MonoBehaviour
{
    public Transform r_PickUpTarget;

    GameObject r_PickUpObject;
    bool m_CanInteract = false;

    CharacterInputs r_CharacterIns;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterIns = GetComponent<CharacterInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckInteract();
    }

    void CheckInteract()
    {
        if (r_CharacterIns.m_Interact == true && m_CanInteract == true)
        {
            Debug.Log("Pick up Payload");
            PickUp();
        }
    }

    void PickUp()
    {
        r_PickUpObject.transform.position = r_PickUpTarget.position;
        r_PickUpObject.transform.rotation = r_PickUpTarget.rotation;
        r_PickUpObject.transform.SetParent(r_PickUpTarget);
    }


    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Payload"))
        {
            r_PickUpObject = col.gameObject;
            m_CanInteract = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Payload"))
        {
            r_PickUpObject = null;
            m_CanInteract = false;
        }
    }
}
