﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInputs : MonoBehaviour
{
    public Vector3 m_MousePos;


    public float m_CharacterMove = 0f;
    public float m_CharacterRotation = 0f;
    public bool m_Boost = false;
    public bool m_Interact = false;


    //public Vector3 m_Loo

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetMotion();
        GetRotation();
        GetMousePos();
        GetBoost();
        GetInteract();
    }

    void GetMotion()
    {
        m_CharacterMove = Input.GetAxisRaw("Vertical");
    }

    void GetRotation()
    {
        m_CharacterRotation = Input.GetAxisRaw("Horizontal");
    }

    void GetMousePos()
    {
        m_MousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void GetBoost()
    {
        if (Input.GetButtonDown("Boost") == true)
        {
            m_Boost = true;
        }
        else
        {
            m_Interact = false;
        }
    }

    void GetInteract()
    {
        if (Input.GetButtonDown("Interact") == true)
        {
            m_Interact = true;
        }
        else
        {
            m_Interact = false;
        }
    }
}
