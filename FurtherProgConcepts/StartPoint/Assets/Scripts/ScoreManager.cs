﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Manager class for keeping track of score
public class ScoreManager : MonoBehaviour
{
    // private variables
    int m_Score = 0; // Stores current score
    
    // Start is called before the first frame update
    void Start()
    {
        // Not used
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Score: " + m_Score); // Writes Score to console
        //This can later be replaced with functionality to update a UI
    }

    // Public function that enables the score to be updated 
    // Function uses a parameter to allow score to be added to total
    public void AddScore(int points)
    {
        m_Score = m_Score + points; // Adds parameter, points, to total score
    }
}
