﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Decides when an enemy dies and updates score
public class EnemyDie : MonoBehaviour
{
    // Private variable
    ScoreManager r_ScoreMan; //Stores refernce to the score manager

    // Start is called before the first frame update
    void Start()
    {
        // Finds the GameManager gameobject and stores the ScoreManager component on it into the refernce type variable
        r_ScoreMan = GameObject.Find("GameManager").GetComponent<ScoreManager>();
    }

    // Checks for collisions with other game objects on a Fixed Update
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Bullet") // Checks to see if colliding object is a bullet
        {
            r_ScoreMan.AddScore(10); // Adds 10 to the score
            Destroy(col.gameObject); // Destoys the bullet that hit it 
            Destroy(gameObject); // Destroys itself
        }
    }
}
