﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        IAmAFunction();
    }

    // Update is called once per frame
    void Update()
    {
        bool stateOfAwesome = IReturnABool();
    }

    void IAmAFunction()
    {
        //In here cool stuff can happen
    }

    void IHaveAParameter(int number)
    {
        number = number * 2;
    }

    bool IReturnABool()
    {
        bool isAwesome = true;
        return isAwesome;
    }
}
