﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    [SerializeField]
    float m_MoveSpeed = 3.5f;
    [SerializeField]
    float m_AttackSpeed = 5f;
    [SerializeField]
    float m_FieldOfView = 100f;
    [SerializeField]
    Transform[] r_Waypoints;
    

    int m_ActiveWaypoint = 0;
    NavMeshAgent r_NavAgent;
    SphereCollider r_TriggerCol;
    Vector3 m_PlayerLastSeen;
    bool m_PlayerInRange;
    bool m_PlayerSeen;

    enum EnemyState {intoAttack, attack,  intoPatrol , patrol, intoSearch, search};
    EnemyState m_EnemyState;

    // Private variables
    Transform r_PlayerLoc; // Stores referece to the player's ship transform
    Vector3 m_LastFramePos; // Stores position of enemy at end of frame

    // Start is called before the first frame update
    void Start()
    {
        m_EnemyState = EnemyState.patrol;
        r_NavAgent = GetComponent<NavMeshAgent>();
        r_TriggerCol = GetComponent<SphereCollider>();
        r_PlayerLoc = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        // Checks static variable in Player Die script to see if player is alive
        if (PlayerDie.s_PlayerAlive == false)
        {
            r_NavAgent.SetDestination(transform.position);
            r_NavAgent.isStopped = true;
            transform.position = m_LastFramePos;
        }
    }

    void FixedUpdate()
    {
        // Checks static variable in Player Die script to see if player is alive
        if (PlayerDie.s_PlayerAlive == true)
        {
            AIStateMachine(); // Runs Statemachine
        }
    }

    // Moves enemy towards the player
    void MoveTo(Vector3 target)
    {
        r_NavAgent.SetDestination(target);
        m_LastFramePos = transform.position;
    }

    void AIStateMachine()
    {
        Debug.Log(m_EnemyState);
        switch (m_EnemyState)
        {
            case EnemyState.intoAttack:
                CancelInvoke("EndSearch");
                r_NavAgent.speed = m_AttackSpeed;
                m_EnemyState = EnemyState.attack;
                break;
            case EnemyState.attack:
                Sight();
                MoveTo(r_PlayerLoc.position);
                break;
            case EnemyState.intoPatrol:
                ReEnterPatrol();
                r_NavAgent.speed = m_MoveSpeed;
                m_EnemyState = EnemyState.patrol;
                break;
            case EnemyState.patrol:
                Sight();
                Patrol();
                break;
            case EnemyState.intoSearch:
                Invoke("EndSearch", 4);
                m_PlayerSeen = false;
                m_EnemyState = EnemyState.search;
                break;
            case EnemyState.search:
                Sight();
                MoveTo(m_PlayerLastSeen);
                break;
            default:
                m_EnemyState = EnemyState.patrol;
                break;
        }
    }

    void EndSearch()
    {
        m_EnemyState = EnemyState.intoPatrol;
    }

    void ReEnterPatrol()
    {
        float finalDist = Vector3.Distance(transform.position, r_Waypoints[0].position);
        m_ActiveWaypoint = 0;

        for (int i = 0; i < r_Waypoints.Length; i++)
        {
            float dist = Vector3.Distance(transform.position, r_Waypoints[i].position);
            if (dist < finalDist)
            {
                finalDist = dist;
                m_ActiveWaypoint = i;
            }
        }
    }

    void Patrol()
    {
        if (Vector3.Distance(transform.position, r_Waypoints[m_ActiveWaypoint].position) > 0.1f)
        {
            MoveTo(r_Waypoints[m_ActiveWaypoint].position);
        }
        else
        {
            if (r_Waypoints.Length - 1 > m_ActiveWaypoint)
            {
                m_ActiveWaypoint++;
            }
            else
            {
                m_ActiveWaypoint = 0;
            }
        }
    }

    void Sight()
    {
        if (m_PlayerInRange == true)
        {
            Vector3 dir = r_PlayerLoc.position - transform.position;
            float angle = Vector3.Angle(dir, transform.forward);

            if (angle < (m_FieldOfView * 0.5f))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, dir.normalized, out hit, r_TriggerCol.radius))
                {
                    if (hit.collider.tag == "Player")
                    {
                        m_PlayerSeen = true;
                        Debug.DrawRay(transform.position, dir, Color.red, 2);
                        m_PlayerLastSeen = r_PlayerLoc.position;
                        m_EnemyState = EnemyState.attack;
                    }
                    else if(m_PlayerSeen == true)
                    {
                        m_EnemyState = EnemyState.intoSearch;
                    }
                }
                
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            m_PlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            m_PlayerInRange = false;
        }
    }
}
