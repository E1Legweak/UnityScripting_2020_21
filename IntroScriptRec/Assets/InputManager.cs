﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    //Player move speed
    float moveSpeed = 10f;
    //Stores aim state
    bool isAiming = false;
    //Stores current item ID
    int activeItemID = 0;
    //Time item cycle pressed
    float timeOfPressItems = 0f;
    //Cycle interval of items
    float cycleIterval = 1f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        MoveSpeed();
        Interact();
        Shooting();
        WeaponAttachments();
        ItemSelect();
    }

    //Moves player based upon input
    void Movement()
    {
        //Moves player forward
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("Moving Forward");
        }
        //Moves Player Backwards
        if (Input.GetKey(KeyCode.S))
        {
            Debug.Log("Moving Backward");
        }
        //Moves Player Left
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("Moving Left");
        }
        //Moves Player Right
        if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("Moving Right");
        }
    }

    //Changes player move speed
    void MoveSpeed()
    {
        //Starts sprint
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            moveSpeed = 15f;
            Debug.Log("Move Speed = " + moveSpeed);
        }
        //Ends sprint
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            moveSpeed = 10f;
            Debug.Log("Move Speed = " + moveSpeed);
        }
    }

    //Interacts with objects
    void Interact()
    {
        //Starts interact
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Start Interaction");
        }
        //Ends interact
        if (Input.GetKeyUp(KeyCode.E))
        {
            Debug.Log("Stop Interaction");
        }
    }

    //Fires and aims weapon
    void Shooting()
    {
        //Reload
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Reload");
        }
        else
        {
            //Shooting
            if (Input.GetMouseButton(0))
            {
                Debug.Log("Shot Weapon");
            }
            else if (Input.GetMouseButtonDown(2))
            {
                Debug.Log("Switch Shoulder");
            }

            //Starts Aim
            if (Input.GetMouseButtonDown(1))
            {
                isAiming = true;
                Debug.Log("Aiming = " + isAiming);
            }
            //Ends Aim
            if (Input.GetMouseButtonUp(1))
            {
                isAiming = false;
                Debug.Log("Aiming = " + isAiming);
            }

            //Toogle Aim
            if (Input.GetKeyDown(KeyCode.LeftAlt))
            {
                if (isAiming == true)
                {
                    isAiming = false;
                }
                else
                {
                    isAiming = true;
                }
                Debug.Log("Aiming = " + isAiming);
            }
        }
    }

    //Change Weapon Attachments
    void WeaponAttachments()
    {
        if (isAiming == true)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                Debug.Log("Switch Muzzle");
            }
            else if (Input.GetKeyDown(KeyCode.Y))
            {
                Debug.Log("Switch Underbarrel");
            }
            else if (Input.GetKeyDown(KeyCode.B))
            {
                Debug.Log("Switch Scope");
            }
        }
    }

    //Selects items
    void ItemSelect()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            timeOfPressItems = Time.time;
            Debug.Log("Active Item is item " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            activeItemID = 0;
            Debug.Log("Active Item is item " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            activeItemID = 1;
            Debug.Log("Active Item is item " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            activeItemID = 2;
            Debug.Log("Active Item is item " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            activeItemID = 3;
            Debug.Log("Active Item is item " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            activeItemID = 4;
            Debug.Log("Active Item is item " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            activeItemID = 5;
            Debug.Log("Active Item is item " + activeItemID);
        }

        if (Input.GetKey(KeyCode.Alpha4))
        {
            CycleItems();
        }
    }

    //Cycles through items
    void CycleItems()
    {
        Debug.Log("Cycle Items");
        if (Time.time > (timeOfPressItems + cycleIterval))
        {
            if (activeItemID < 5)
            {
                activeItemID = activeItemID + 1;
            }
            else
            {
                activeItemID = 0;
            }
            timeOfPressItems = Time.time;
            Debug.Log("Active Item is item " + activeItemID);
        }
    }
}
