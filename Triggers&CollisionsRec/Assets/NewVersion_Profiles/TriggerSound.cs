﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSound : MonoBehaviour
{
    //Reference to audio source 
    public AudioSource r_AudioS;

    //Ensures play of sound
    bool m_hasPlayed = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    //Plays sound on player entering trigger
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && m_hasPlayed == false)
        {
            r_AudioS.Play();
            m_hasPlayed = true;
        }
    }
}
