﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowEvent : MonoBehaviour
{
    //Force with which the object is thrown
    public float m_Force = 25f;
    
    //Prevent sound from playing multiple times
    bool m_hasPlayed = false;

    //Refernces to attached components
    Rigidbody r_Rigidb;
    AudioSource r_AudioS;

    // Start is called before the first frame update
    void Start()
    {
        r_Rigidb = GetComponent<Rigidbody>();
        r_AudioS = GetComponent<AudioSource>();
    }

    //Receives broadcast from trigger
    void RunEvent()
    {
        r_Rigidb.useGravity = true;
        r_Rigidb.AddForce(transform.forward * m_Force, ForceMode.Impulse);
    }

    //Runs collision event
    void OnCollisionEnter()
    {
        if (m_hasPlayed == false)
        {
            r_AudioS.Play();
            m_hasPlayed = true;
        }
    }
}
