﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    //Reference to receiving object
    public GameObject r_EventReceiver;
    //Ensures single broadcast of event
    bool m_hasPlayed = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    //Broadcasts event based upon player entering script
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && m_hasPlayed == false)
        {
            r_EventReceiver.BroadcastMessage("RunEvent");
            m_hasPlayed = true;
        }
    }
}
