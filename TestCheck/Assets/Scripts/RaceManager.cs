﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceManager : MonoBehaviour
{
    public GameObject r_SpawnPoint1;
    public GameObject r_SpawnPoint2;
    public GameObject r_CarRed;
    public GameObject r_CarBlue;

    public int m_NumberOfCarsFinished = 0;
    public int m_Takings = 0;

    public bool m_RedCarSpawned;
    public bool m_BlueCarSpawned;

    InputManager r_InputManager;


    // Start is called before the first frame update
    void Start()
    {
        r_InputManager = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        CreateCars();
    }

    void CreateCars()
    {
        if (r_InputManager.m_RaceOpen == true)
        {
            if (r_InputManager.m_SpawnRedCar == true && m_RedCarSpawned == false)
            {
                Debug.Log("Spawn red car");
                m_RedCarSpawned = true;
                Instantiate(r_CarRed, r_SpawnPoint1.transform.position, r_SpawnPoint1.transform.rotation);
            }
            if (r_InputManager.m_SpawnBlueCar == true && m_BlueCarSpawned == false)
            {
                Debug.Log("Spawn blue car");
                m_BlueCarSpawned = true;
                Instantiate(r_CarBlue, r_SpawnPoint2.transform.position, r_SpawnPoint2.transform.rotation);
            }
        }
        else
        {
            m_NumberOfCarsFinished = 0;
            m_Takings = 0;
        }
    }
}
