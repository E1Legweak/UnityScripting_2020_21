﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public bool m_RaceOpen;
    public bool m_SpawnRedCar;
    public bool m_SpawnBlueCar;
    public bool m_RaceStart;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OpenRace();
        SpawnCars();
        StartRace();
    }

    void OpenRace()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (m_RaceOpen)
            {
                m_RaceOpen = false;
            }
            else
            {
                m_RaceOpen = true;
            }
            Debug.Log("Race Open = " + m_RaceOpen);
        }
    }

    void SpawnCars()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_SpawnRedCar = true;
        }
        else
        {
            m_SpawnRedCar = false;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_SpawnBlueCar = true;
        }
        else
        {
            m_SpawnBlueCar = false;
        }
    }

    void StartRace()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_RaceStart = true;
        }
        else
        {
            m_RaceStart = false;
        }
    }
}
