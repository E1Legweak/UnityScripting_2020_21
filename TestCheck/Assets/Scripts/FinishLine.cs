﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    public RaceManager r_RaceManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "CarRed")
        {
            Destroy(col.gameObject, 2);
            r_RaceManager.m_RedCarSpawned = false;
            r_RaceManager.m_Takings = r_RaceManager.m_Takings + 10000;
            r_RaceManager.m_NumberOfCarsFinished++;
            Debug.Log(r_RaceManager.m_NumberOfCarsFinished + " cars have raced today. The bookmakers have made £" + r_RaceManager.m_Takings + " in profit.");
        }
        if (col.gameObject.tag == "CarBlue")
        {
            Destroy(col.gameObject, 2);
            r_RaceManager.m_BlueCarSpawned = false;
            r_RaceManager.m_Takings = r_RaceManager.m_Takings + 20000;
            r_RaceManager.m_NumberOfCarsFinished++;
            Debug.Log(r_RaceManager.m_NumberOfCarsFinished + " cars have raced today. The bookmakers have made £" + r_RaceManager.m_Takings + " in profit.");
        }
    }
}
