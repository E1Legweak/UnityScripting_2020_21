﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPower : MonoBehaviour
{
    Rigidbody r_RigidB;
    bool m_Go;
    float m_Acceleration;
    InputManager r_InputManager;

    float m_JumpTime;

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB = GetComponent<Rigidbody>();
        r_InputManager = GameObject.FindGameObjectWithTag("StartLine").GetComponent<InputManager>();
        m_Acceleration = Random.Range(0.0f, 20.0f);
        m_JumpTime = Time.time + Random.Range(0.5f, 1f);
    }

    void RandomJump()
    {
        if (m_JumpTime < Time.time)
        {
            m_JumpTime = Time.time + Random.Range(0.5f, 3f);
            r_RigidB.AddForce(transform.up * 1.5f, ForceMode.Impulse);
        }
    }

    // Update is called once per frame
    void Update()
    {
        RaceStart();
        RandomJump();
    }

    void FixedUpdate()
    {
        CarAcceleration();
    }

    void RaceStart()
    {
        if (r_InputManager.m_RaceStart == true)
        {
            m_Go = true;
        }
    }

    void CarAcceleration()
    {
        if (m_Go == true)
        {
            if (m_Acceleration < 20)
            {
                m_Acceleration = m_Acceleration + 0.1f;
            }

            r_RigidB.AddForce(transform.forward * m_Acceleration, ForceMode.Acceleration);
        }
    }
}
