﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : MonoBehaviour
{
    public Vector3 m_PlayerMotion;
    public Vector3 m_MousePositionInWorld;
    public bool m_Jump = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveInputs();
        MousePosInScene();
        JumpInput();
    }

    void MoveInputs()
    {
        m_PlayerMotion = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        m_PlayerMotion = Vector3.Normalize(m_PlayerMotion);
    }

    void MousePosInScene()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 25, Color.red);

        if (Physics.Raycast(ray, out hit) == true)
        {
            m_MousePositionInWorld = hit.point;
        }
    }

    void JumpInput()
    {
        if (Input.GetButtonDown("Jump"))
        {
            m_Jump = true;
        }
        else
        {
            m_Jump = false;
        }
    }
}
