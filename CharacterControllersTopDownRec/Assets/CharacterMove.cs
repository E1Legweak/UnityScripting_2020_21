﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour
{
    public float m_MoveSpeed = 10f;
    public float m_Gravity = 10f;

    CharacterController r_CharacterCon;
    PlayerInputs r_PlayerInput;
    bool m_IsGrounded = false;
    bool m_IsJumping = false;

    // Start is called before the first frame update
    void Start()
    {
        r_CharacterCon = GetComponent<CharacterController>();
        r_PlayerInput = GetComponent<PlayerInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        GroundCheck();
        Movement(r_PlayerInput.m_PlayerMotion);
    }

    void Movement(Vector3 moveVector)
    {
        if (m_IsJumping == true)
        {
            moveVector.y += (m_Gravity / 10);
        }
        else if (m_IsGrounded == false)
        {
            moveVector.y += -(m_Gravity / 10);
        }

        r_CharacterCon.Move(moveVector * m_MoveSpeed * Time.deltaTime);
    }

    void GroundCheck()
    {
        Ray ray = new Ray(transform.position, -transform.up);

        if (Physics.Raycast(ray, 0.2f) == true)
        {
            m_IsGrounded = true;
        }
        else
        {
            m_IsGrounded = false;
        }
    }

    void Jump()
    {
        if (r_PlayerInput.m_Jump == true && m_IsGrounded == true)
        {
            m_IsJumping = true;
            CancelInvoke("StopJump");
            Invoke("StopJump", 0.2f);
        }
    }

    void StopJump()
    {
        m_IsJumping = false;
    }
}
