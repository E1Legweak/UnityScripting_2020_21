﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLook : MonoBehaviour
{
    public Transform m_LookTarget;
    PlayerInputs r_PlayerIns;

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GetComponent<PlayerInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        SetLookTarget();
    }

    void SetLookTarget()
    {
        float x = r_PlayerIns.m_MousePositionInWorld.x;
        float z = r_PlayerIns.m_MousePositionInWorld.z;

        m_LookTarget.position = new Vector3(x, transform.position.y, z);
        transform.LookAt(m_LookTarget);
    }
}
