﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public float m_Offset = 5f;
    public float m_Smoothness = 1f;
    public Transform r_FollowTarget;

    PlayerInputs r_PlayerIns;
    Vector3 m_TargetPos = Vector3.zero;
    Vector3 m_CurrentVel = Vector3.zero;
    float m_xCam = 0f;
    float m_zCam = 0f;

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInputs>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        SetCameraTarget();
        Follow();
    }

    void Follow()
    {
        //transform.position = m_TargetPos;
        transform.position = Vector3.SmoothDamp(transform.position, m_TargetPos, ref m_CurrentVel, m_Smoothness * Time.deltaTime);
    }

    void SetCameraTarget()
    {
        if (r_PlayerIns.m_PlayerMotion.x > 0)
        {
            m_xCam = m_Offset;
        }
        else if (r_PlayerIns.m_PlayerMotion.x < 0)
        {
            m_xCam = - m_Offset;
        }

        if (r_PlayerIns.m_PlayerMotion.z > 0)
        {
            m_zCam = m_Offset;
        }
        else if (r_PlayerIns.m_PlayerMotion.z < 0)
        {
            m_zCam = -m_Offset;
        }

        m_TargetPos = r_FollowTarget.position + new Vector3(m_xCam, 0, m_zCam);
    }
}
