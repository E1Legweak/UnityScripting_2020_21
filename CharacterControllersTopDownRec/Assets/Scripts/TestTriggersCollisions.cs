﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTriggersCollisions : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    //Debugs on Trigger Entered by Player
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            Debug.Log("Triggered");
        }
    }

    //Debugs on collision with Player
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("Collided");
        }
    }
}
