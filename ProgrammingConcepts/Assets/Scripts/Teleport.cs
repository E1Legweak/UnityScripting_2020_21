﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    //Stors new location
    Vector3 newLoc = Vector3.zero;

    //Generates and sets random location
    public void ChangeLocation()
    {
        newLoc = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
        transform.position = newLoc;
    }
}
