﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Visability : MonoBehaviour
{
    public MeshRenderer m_rend;

    // Start is called before the first frame update
    void Start()
    {
        m_rend = GetComponent<MeshRenderer>();
    }

    public void ToggleVisability()
    {
        m_rend.enabled = !m_rend.enabled;
    }
}
