﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialChange : MonoBehaviour
{
    public Material green;
    public Material red;

    MeshRenderer m_rend;
    bool isRed = false;

    // Start is called before the first frame update
    void Start()
    {
        m_rend = GetComponent<MeshRenderer>();
        SetIsRed();
    }

    void SetIsRed()
    {
        if (m_rend.material.color == green.color)
        {
            isRed = false;
        }
        else
        {
            isRed = true ;
        }
    }

    public void ChangeMaterial()
    {
        if (isRed == false)
        {
            m_rend.material = red;
        }
        else
        {
            m_rend.material = green;
        }
        isRed = !isRed;
    }
}
