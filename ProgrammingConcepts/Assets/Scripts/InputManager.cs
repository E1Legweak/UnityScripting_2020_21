﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    Teleport m_Teleport;
    MaterialChange m_MatChange;
    Visability m_Visibility;

    // Start is called before the first frame update
    void Start()
    {
        m_Teleport = GetComponent<Teleport>();
        m_MatChange = GetComponent<MaterialChange>();
        m_Visibility = GetComponent<Visability>();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeColour();
        Disappear();
        Teleport();
    }

    void ChangeColour()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_MatChange.ChangeMaterial();
        }
    }

    void Disappear()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_Visibility.ToggleVisability();
        }
    }

    void Teleport()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            m_Teleport.ChangeLocation();
        }
    }
}
