﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    //Stores health
    int health = 100;
    //Speed of movement
    float moveSpeed = 10f;
    //Stores player living state
    bool playerAlive = true;

    void CheckHealth()
    {
        if (health < 20 && health > 0)
        {
            moveSpeed = 3.5f;
        }
        else if (health <= 0)
        {
            playerAlive = false;
        }
        else
        {
            moveSpeed = 10f;
        }
    }
}
