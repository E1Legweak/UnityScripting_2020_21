﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseScript : MonoBehaviour
{
    // Awake is called before Start and the first frame update
    void Awake()
    {
        Debug.Log("Awake");
    }
    
    // Start is called after Awake and before the first frame update
    void Start()
    {
        Debug.Log("Start");
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update");
    }
    
    // LateUpdate is called once per frame after Update
    void LateUpdate()
    {
        Debug.Log("LateUpdate");
    }

    // FixetUpdate is called every specified interval of time
    void FixedUpdate()
    {
        Debug.Log("FixedUpdate");
    }

}
