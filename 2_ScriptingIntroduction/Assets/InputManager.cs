﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    bool isAiming = false;
    int activeItemID = 0;
    float timeOfPressItems;
    float cycleInterval = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Shooting();
        MoveSpeed();
        Interact();
        SelectWeapon();
        Items();
        PlayerStance();
        WeaponAttachments();
    }

    //Player Movement 
    void Movement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("Move Forward");
        }
        if (Input.GetKey(KeyCode.S))
        {
            Debug.Log("Move Backward");
        }
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("Move Left");
        }
        if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("Move Right");
        }
    }

    //Player Speed Changes 
    void MoveSpeed()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Debug.Log("Start Sprinting");
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            Debug.Log("Stop Sprinting");
        }
    }

    //Checks for player interaction
    void Interact()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Interact with objects");
        }
    }

    //Fires weapons on 
    void Shooting()
    {
        //Reload
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Reload");
        }
        else
        {
            //Shoot
            if (Input.GetMouseButton(0))
            {
                Debug.Log("Shot Weapon");
            }
            if (Input.GetMouseButtonDown(2))
            {
                Debug.Log("Switch Shoulder");
            }

            //Aiming Modes
            if (Input.GetMouseButtonDown(1))
            {
                isAiming = true;
                Debug.Log("Aiming = " + isAiming);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                isAiming = false;
                Debug.Log("Aiming = " + isAiming);
            }
            //Toggle Aiming
            if (Input.GetKeyDown(KeyCode.LeftAlt))
            {
                if (isAiming == true)
                {
                    isAiming = false;
                }
                else
                {
                    isAiming = true;
                }
                Debug.Log("Aiming = " + isAiming);
            }
        }
    }

    //Chooses weapon to use
    void SelectWeapon()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.Log("Select Primary Weapon");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Debug.Log("Select Secondary Weapon");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Debug.Log("Select Sidearm");
        }

        if (Input.mouseScrollDelta.y < 0)
        {
            Debug.Log("Select Next Weapon");
        }
        else if (Input.mouseScrollDelta.y > 0)
        {
            Debug.Log("Select Last Weapon");
        }
    }

    //Select and use items
    void Items()
    {
        if (Input.GetKey(KeyCode.G))
        {
            Debug.Log("Use Item " + activeItemID);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Debug.Log("Pick Item = " + activeItemID);
            timeOfPressItems = Time.time;
        }

        if (Input.GetKey(KeyCode.Alpha4))
        {
            CycleItems();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            activeItemID = 0;
            Debug.Log("Pick Item = " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            activeItemID = 1;
            Debug.Log("Pick Item = " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            activeItemID = 2;
            Debug.Log("Pick Item = " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            activeItemID = 3;
            Debug.Log("Pick Item = " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            activeItemID = 4;
            Debug.Log("Pick Item = " + activeItemID);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            activeItemID = 5;
            Debug.Log("Pick Item = " + activeItemID);
        }
    }

    void CycleItems()
    {
        Debug.Log("Cycle Items");
        if (Time.time > (timeOfPressItems + cycleInterval))
        {
            if (activeItemID < 5)
            {
                activeItemID = activeItemID + 1;
            }
            else
            {
                activeItemID = 0;
            }
            timeOfPressItems = Time.time;
            Debug.Log("Pick Item = " + activeItemID);
        }
    }

    //Changes Player Stance
    void PlayerStance()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("Go Prone");
        }
        else if (Input.GetKeyDown(KeyCode.N))
        {
            Debug.Log("Stand");
        }
    }

    //Change Weapon Attachments
    void WeaponAttachments()
    {
        if (isAiming == true)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                Debug.Log("Switch Muzzle");
            }
            else if (Input.GetKeyDown(KeyCode.Y))
            {
                Debug.Log("Switch Underbarrel");
            }
            else if (Input.GetKeyDown(KeyCode.B))
            {
                Debug.Log("Switch Scope");
            }
        }
    }

}
