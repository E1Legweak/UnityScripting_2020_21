﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animate : MonoBehaviour
{
    Animation r_Anim;

    // Start is called before the first frame update
    void Start()
    {
        r_Anim = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (r_Anim.isPlaying == false)
            {
                r_Anim.Play();
            }
            else
            {
                r_Anim.Stop();
            }
        }
    }
}
