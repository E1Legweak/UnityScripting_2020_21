﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public bool m_AllowSpawn;
    public bool m_SpawnBlue;
    public bool m_SpawnYellow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SpawnInputs();
    }

    void SpawnInputs()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (m_AllowSpawn == true)
            {
                m_AllowSpawn = false;
            }
            else
            {
                m_AllowSpawn = true;
            }

            Debug.Log("Spawning is enabled: " + m_AllowSpawn);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_SpawnBlue = true;
        }
        else
        {
            m_SpawnBlue = false;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_SpawnYellow = true;
        }
        else
        {
            m_SpawnYellow = false;
        }
    }

}
