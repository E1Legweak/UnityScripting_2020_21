﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour
{
    InputManager r_InputManager;
    public GameObject r_BlueBox;
    public GameObject r_YellowBox;
    public GameObject r_SpawnPoint;
    int m_BlueBoxesSpawned;
    int m_YellowBoxesSpawned;
    public int m_StoredBoxes;

    // Start is called before the first frame update
    void Start()
    {
        r_InputManager = GetComponent<InputManager>();
        m_BlueBoxesSpawned = 0;
        m_YellowBoxesSpawned = 0;
        m_StoredBoxes = 0;
    }

    // Update is called once per frame
    void Update()
    {
        SpawnBoxes();
    }

    void SpawnBoxes()
    {
        if (r_InputManager.m_AllowSpawn == true)
        {
            if (r_InputManager.m_SpawnBlue == true)
            {
                Instantiate(r_BlueBox, r_SpawnPoint.transform.position, r_SpawnPoint.transform.rotation);
                m_BlueBoxesSpawned++;
                Debug.Log("Amount of blue boxes spawned is: " + m_BlueBoxesSpawned);
            }
            if (r_InputManager.m_SpawnYellow == true)
            {
                Instantiate(r_YellowBox, r_SpawnPoint.transform.position, r_SpawnPoint.transform.rotation);
                m_YellowBoxesSpawned++;
                Debug.Log("Amount of yellow boxes spawned is: " + m_YellowBoxesSpawned);
            }
        }
    }
}
