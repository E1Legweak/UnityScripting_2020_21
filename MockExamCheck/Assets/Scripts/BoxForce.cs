﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxForce : MonoBehaviour
{
    public float m_BoxSpeed = 2.5f;
    public Rigidbody r_RigidB;
    BoxSpawner r_BoxSpawner;

    // Start is called before the first frame update
    void Start()
    {
        r_BoxSpawner = GameObject.Find("BoxSpawner").GetComponent<BoxSpawner>();
    }

    void FixedUpdate()
    {
        r_RigidB.AddForce(transform.forward * m_BoxSpeed, ForceMode.Force);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "BoxExitPoint")
        {
            r_BoxSpawner.m_StoredBoxes++;
            Debug.Log("We have stored a total of " + r_BoxSpawner.m_StoredBoxes + " boxes");
            Destroy(gameObject, 0.1f);
        }
    }
}
