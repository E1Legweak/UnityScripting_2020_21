﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float inputX;
    float inputZ;
    float speed = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        inputX = Input.GetAxis("Horizontal");
        inputZ = Input.GetAxis("Vertical");


        float x = transform.position.x + (inputX * speed);
        float z = transform.position.z + (inputZ * speed);

        transform.position = new Vector3(Mathf.Clamp(x, -4.6f, 4.6f), 0, Mathf.Clamp(z, -4, 11.75f));
    }
}
