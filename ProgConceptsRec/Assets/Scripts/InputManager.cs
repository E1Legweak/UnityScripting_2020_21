﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    //Reference Variables
    public Teleport r_Teleport;
    public VisibilityChange r_VisChange;
    public MaterialChange r_MatChange;

    // Start is called before the first frame update
    void Start()
    {
        //Setting references
        r_VisChange = GetComponent<VisibilityChange>();
        r_MatChange = GetComponent<MaterialChange>();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeColour();
        Dissappear();
        Teleport();
    }

    void ChangeColour()
    {
        if (Input.GetMouseButtonDown(0))
        {
            r_MatChange.ChangeMaterial();
            Debug.Log("Change Colour");
        }
    }

    void Dissappear()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            r_VisChange.ToggleVisability();
            Debug.Log("Toggle Visability");
        }
    }

    void Teleport()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            r_Teleport.ChangeLocation();
            Debug.Log("Teleport");
        }
    }
}
