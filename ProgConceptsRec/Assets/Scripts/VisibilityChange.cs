﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibilityChange : MonoBehaviour
{
    public MeshRenderer r_MeshRend;

    // Start is called before the first frame update
    void Start()
    {
        r_MeshRend = GetComponent<MeshRenderer>();
    }

    public void ToggleVisability()
    {
        r_MeshRend.enabled = !r_MeshRend.enabled;
    }
}
