﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    //Stores new teleport location
    Vector3 newLoc = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ChangeLocation()
    {
        newLoc = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
        transform.position = newLoc;
    }
}
