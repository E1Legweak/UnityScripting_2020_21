﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialChange : MonoBehaviour
{
    public Material r_Green;
    public Material r_Red;

    bool isGreen = false;

    MeshRenderer r_MeshRend;

    // Start is called before the first frame update
    void Start()
    {
        r_MeshRend = GetComponent<MeshRenderer>();
        CheckColour();
    }

    void CheckColour()
    {
        if (r_MeshRend.material.color == r_Green.color)
        {
            isGreen = true;
        }
        else
        {
            isGreen = false;
        }
    }

    public void ChangeMaterial()
    {
        if (isGreen == true)
        {
            r_MeshRend.material = r_Red;
        }
        else
        {
            r_MeshRend.material = r_Green;
        }
        isGreen = !isGreen;
    }
}
