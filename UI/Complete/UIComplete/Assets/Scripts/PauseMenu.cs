﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject r_PauseMenu;
    PlayerInputs r_PlayerInputs;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        r_PauseMenu.SetActive(false);
        r_PlayerInputs = GameObject.FindObjectOfType<PlayerInputs>();
    }

    // Update is called once per frame
    void Update()
    {
        PauseGame();
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }

    void PauseGame()
    {
        if (r_PlayerInputs.IsPaused == true)
        {
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                r_PauseMenu.SetActive(false);
            }
            else
            {
                Time.timeScale = 0;
                r_PauseMenu.SetActive(true);
            }
        }
    }
}
