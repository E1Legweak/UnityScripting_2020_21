﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Propels the bullet forward
public class BulletProjectile : MonoBehaviour
{
    // Variables accessabile through the editor
    [SerializeField]
    Rigidbody r_RigidB; // Variable to score the rigidbidy
    [SerializeField]
    float m_BulletSpeed = 5f; // Variable to allow the designer to alter speed in editor

    // Start is called before the first frame update
    void Start()
    {
        r_RigidB.AddForce(transform.forward * m_BulletSpeed, ForceMode.VelocityChange); // Applies force along the bullet's z (forward) axis
        Destroy(gameObject, 2); // Waits 2 seconds, then destroys the bullet
    }

    // Update is called once per frame
    void Update()
    {
        // Not used
    }
}
