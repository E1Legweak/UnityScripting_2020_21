﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Moves ship
// Requires a CharacterController Component and PlayerInputs script to be added to the same GameObject
public class ShipMovement : MonoBehaviour
{
    // Variable private but made available in inspector so speed can be tuned easily
    [SerializeField]
    [Range(0.0f, 1f)]
    float m_ShipSpeed = 1f; // Ship speed between 0 and 1

    // Private variables
    Vector3 m_ClampedMotion = Vector3.zero; // Used to store a clamped version of the motion vector
    PlayerInputs r_PlayerIns; // Stores refernce to Player Inputs script
    CharacterController r_CharacterCon; // Stores reference to Character Controller Component

    // Start is called before the first frame update
    void Start()
    {
        r_PlayerIns = GetComponent<PlayerInputs>(); // Assigns Player Inputs script to reference variable  
        r_CharacterCon = GetComponent<CharacterController>(); // Assigns Character Controller to reference variable
    }

    // Update is called once per frame
    void Update()
    {
        // Checks static variable in Player Die script to see if player is alive
        if (PlayerDie.s_PlayerAlive == true)
        {
            PlayerMove(); // If player is alive runs PlayerMove, else does nothing
        }
    }

    // Function that moves player
    void PlayerMove()
    {
        m_ClampedMotion = r_PlayerIns.PlayerMotion; // Makes the clamped motion vector equal the motion vector from the inputs script

        if (transform.position.x <= -17) // Checks if player position need clamping left
        {
            m_ClampedMotion.x = Mathf.Clamp(m_ClampedMotion.x, 0, 1); // Clamps input

        }
        else if (transform.position.x >= 17) // Checks if player position need clamping right
        {
            m_ClampedMotion.x = Mathf.Clamp(m_ClampedMotion.x, -1, 0); // Clamps input
        }

        if (transform.position.z <= -9.5f) // Checks if player position need clamping at bottom
        {
            m_ClampedMotion.z = Mathf.Clamp(m_ClampedMotion.z, 0, 1); // Clamps input
        }
        else if (transform.position.z >= 9.5f) // Checks if player position need clamping at top
        {
            m_ClampedMotion.z = Mathf.Clamp(m_ClampedMotion.z, -1, 0); // Clamps input
        }
        
        r_CharacterCon.Move(m_ClampedMotion * m_ShipSpeed * Time.timeScale); // Runs movement using the Character Controller
    }
}
