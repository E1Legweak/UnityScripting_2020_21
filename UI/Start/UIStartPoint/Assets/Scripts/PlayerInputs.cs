﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class to get and manage player inputs
public class PlayerInputs : MonoBehaviour
{
    // Serialized so it can be set in the editor but no where else
    [SerializeField]
    private float m_AxisSmooth = 5f;

    // Property that allows player motion inputs to be got but not set
    private Vector3 m_PlayerMotion;
    public Vector3 PlayerMotion
    {
        get
        {
            return m_PlayerMotion;
        }
    }

    // Property that allows look target to be got but not set
    private Vector3 m_LookTarget;
    public Vector3 LookTarget
    {
        get
        {
            return m_LookTarget;
        }
    }

    // Property that allows look fire state to be got but not set
    private bool m_PrimaryFire;
    public bool PrimaryFire
    {
        get
        {
            return m_PrimaryFire;
        }
    }

    // Private variables
    Vector3 m_PlayerAxisRaw = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        // Not used
    }

    // Update is called once per frame
    // Used to call all inputs as they need to be continually checked
    void Update()
    {
        MoveInputs(); // Runs the 1st function every frame
        LookInputs(); // Runs the 2nd function every frame
        PrimeFire(); // Runs the 3rd function every frame
    }

    // Converts raw inputs from input manager into a normalized and smoothed vector3 for use in player movement
    void MoveInputs()
    {
        // Stores raw input
        m_PlayerAxisRaw = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        // Normailzes vector
        m_PlayerAxisRaw = Vector3.Normalize(m_PlayerAxisRaw);
        // Smooths input
        m_PlayerMotion = Vector3.Lerp(PlayerMotion, m_PlayerAxisRaw, m_AxisSmooth * Time.deltaTime);
    }

    // Converts mouse position from screen space to world space so it can be used as a look target
    // Stores it in a vector
    void LookInputs()
    {
        // Converts mouse position into world space vector
        m_LookTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        // Sets the y of the vector to zero
        m_LookTarget = new Vector3(LookTarget.x, 0, LookTarget.z);
    }

    // Stores primary fire in a boolean. Currently has no other conditions but could be added if nessercary.
    // i.e. could prevent firing while other actions take place
    void PrimeFire()
    {
        // Stores fire state in a bool
        m_PrimaryFire = Input.GetButton("Fire1");
    }

}
